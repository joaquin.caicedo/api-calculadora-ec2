const express = require('express');
const cors = require('cors');
const app = express();
const port = 3000;

app.use(express.json());
app.use(cors({
    origin: '*',
    credentials: true
}));

app.post('/potenciacion', (req, res) => {
    let number1 = req.body.number1;
    let number2 = req.body.number2;

    if ( isNaN(number1) || isNaN(number2) ){
        res.status(400).json({message: 'Números inválidos'});
    } else {
        let potencia = Math.pow(parseFloat(number1), parseFloat(number2));
        res.json({resultado: potencia});
    }
});

app.post('/radicacion', (req, res) => {
    let number1 = req.body.number1;
    let number2 = req.body.number2;

    if ( isNaN(number1) || isNaN(number2) ){
        res.status(400).json({message: 'Números inválidos'});
    } else {
        let potencia = Math.pow(parseFloat(number1), 1/parseFloat(number2));
        res.json({resultado: potencia});
    }
});

app.listen(port, () => {
    console.log(`Servidor en el puerto ${port}`)
});